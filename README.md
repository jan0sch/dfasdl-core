# DFASDL - Data Format and Semantics Description Language

![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/org.dfasdl/dfasdl-core_2.13?server=https%3A%2F%2Foss.sonatype.org)

The DFASDL is a language based upon [XML
Schema](http://www.w3.org/XML/Schema) that can be used to describe data
formats and additionally the semantics of it.

It is used by the Tensei-Data project to describe data structures and to
derive mappings and transformation functions between different structures
automatically.

This repository contains the core module which consists of the xschema 
definition (xsd) and the official specification. Additionally some small
helpers are available to have typed element and attribute names in code.

It is cross build for Scala 2.11, 2.12, 2.13 and 3.

Releases are published via sonatype and should be available on maven
central.

Just add the dependency to your build configuration:

```
libraryDependencies += "org.dfasdl" %% "dfasdl-core" % VERSION
```

