/*
 * Copyright (c) 2014 - 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.dfasdl

import munit.ScalaCheckSuite
import org.scalacheck.{ Arbitrary, Gen }
import org.scalacheck.Prop._

class ElementTest extends ScalaCheckSuite {

  private val element: Gen[Element]                         = Gen.oneOf(Element.values.toList)
  private implicit val arbitraryElement: Arbitrary[Element] = Arbitrary(element)

  private val blacklist: List[String] = Element.values.map(_.tagName.toString())

  property("Element#from must return an empty Option for invalid names") {
    forAll { (s: String) =>
      if (!blacklist.contains(s)) {
        assertEquals(Element.from(s), None)
      }
    }
  }

  property("Attribute#from must return the correct type for a valid name") {
    forAll { (e: Element) =>
      val s: String = e.tagName
      assertEquals(Element.from(s), Option(e))
    }
  }

}
