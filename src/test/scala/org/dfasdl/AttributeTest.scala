/*
 * Copyright (c) 2014 - 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.dfasdl

import munit.ScalaCheckSuite
import org.scalacheck.{ Arbitrary, Gen }
import org.scalacheck.Prop._

class AttributeTest extends ScalaCheckSuite {

  private val attribute: Gen[Attribute]                         = Gen.oneOf(Attribute.values.toList)
  private implicit val arbitraryAttribute: Arbitrary[Attribute] = Arbitrary(attribute)

  private val blacklist: List[String] = Attribute.values.map(_.name.toString())

  property("Attribute#from must return an empty Option for invalid names") {
    forAll { (s: String) =>
      if (!blacklist.contains(s)) {
        assertEquals(Attribute.from(s), None)
      }
    }
  }

  property("Attribute#from must return the correct type for a valid name") {
    forAll { (e: Attribute) =>
      val s: String = e.name
      assertEquals(Attribute.from(s), Option(e))
    }
  }
}
