/*
 * Copyright (c) 2014 - 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.dfasdl

import org.dfasdl.types._

/** An enumeration of all attribute names of the DFASDL.
  */
sealed abstract class Attribute extends Product with Serializable {

  /** The name of the attribute in the DFASDL XML.
    *
    * @return
    *   A string containing the XML name of the attribute.
    */
  def name: XmlAttributeName

}

object Attribute {
  val values: List[Attribute] = List(
    DEFAULT_NUMBER,
    DEFAULT_STRING,
    CORRECT_OFFSET,
    DB_AUTO_INCREMENT,
    DB_COLUMN_NAME,
    DB_FOREIGN_KEY,
    DB_INSERT,
    DB_PRIMARY_KEY,
    DB_SELECT,
    DB_UPDATE,
    DECIMAL_SEPARATOR,
    DEFAULT_ENCODING,
    ENCODING,
    FIXED_SEQUENCE_COUNT,
    FILTER,
    FORMAT,
    JSON_ATTRIBUTE_NAME,
    KEEP_ID,
    LENGTH,
    MAX_DIGITS,
    MAX_LENGTH,
    MAX_PRECISION,
    PRECISION,
    SEMANTIC,
    SEMANTIC_SCHEMA,
    SEQUENCE_MAX,
    SEQUENCE_MIN,
    SOURCE_ID,
    START_SIGN,
    STOP_SIGN,
    STORAGE_PATH,
    TRIM,
    UNIQUE,
    XML_ATTRIBUTE_NAME,
    XML_ATTRIBUTE_PARENT,
    XML_ELEMENT_NAME
  )

  private val mappings: Map[XmlAttributeName, Attribute] = values.map(e => (e.name -> e)).toMap

  /** Return an attribute type corresponding to the given name.
    *
    * @param name
    *   A string containing an XML attribute name.
    * @return
    *   An option to the corresponding DFASDL attribute type.
    */
  def from(name: String): Option[Attribute] =
    // Would be nicer to write this line but it caused problems on 2.11:
    // XmlAttributeName.from(name).toOption.flatMap(n => mappings.get(n))
    XmlAttributeName.from(name) match {
      case Left(_)  => None
      case Right(n) => mappings.get(n)
    }

  case object DEFAULT_NUMBER extends Attribute { override val name: XmlAttributeName = "defaultnum" }

  case object DEFAULT_STRING extends Attribute { override val name: XmlAttributeName = "defaultstr" }

  case object CORRECT_OFFSET extends Attribute {
    override val name: XmlAttributeName = "correct-offset"
  }

  case object DB_AUTO_INCREMENT extends Attribute {
    override val name: XmlAttributeName = "db-auto-inc"
  }

  case object DB_COLUMN_NAME extends Attribute {
    override val name: XmlAttributeName = "db-column-name"
  }

  case object DB_FOREIGN_KEY extends Attribute {
    override val name: XmlAttributeName = "db-foreign-key"
  }

  case object DB_INSERT extends Attribute { override val name: XmlAttributeName = "db-insert" }

  case object DB_PRIMARY_KEY extends Attribute {
    override val name: XmlAttributeName = "db-primary-key"
  }

  case object DB_SELECT extends Attribute { override val name: XmlAttributeName = "db-select" }

  case object DB_UPDATE extends Attribute { override val name: XmlAttributeName = "db-update" }

  case object DECIMAL_SEPARATOR extends Attribute {
    override val name: XmlAttributeName = "decimal-separator"
  }

  case object DEFAULT_ENCODING extends Attribute {
    override val name: XmlAttributeName = "default-encoding"
  }

  case object ENCODING extends Attribute { override val name: XmlAttributeName = "encoding" }

  case object FIXED_SEQUENCE_COUNT extends Attribute { override val name: XmlAttributeName = "count" }

  case object FILTER extends Attribute { override val name: XmlAttributeName = "filter" }

  case object FORMAT extends Attribute { override val name: XmlAttributeName = "format" }

  case object JSON_ATTRIBUTE_NAME extends Attribute {
    override val name: XmlAttributeName = "json-attribute-name"
  }

  case object KEEP_ID extends Attribute { override val name: XmlAttributeName = "keepID" }

  case object LENGTH extends Attribute { override val name: XmlAttributeName = "length" }

  case object MAX_DIGITS extends Attribute { override val name: XmlAttributeName = "max-digits" }

  case object MAX_LENGTH extends Attribute { override val name: XmlAttributeName = "max-length" }

  case object MAX_PRECISION extends Attribute {
    override val name: XmlAttributeName = "max-precision"
  }

  case object PRECISION extends Attribute { override val name: XmlAttributeName = "precision" }

  case object SEMANTIC extends Attribute { override val name: XmlAttributeName = "s" }

  case object SEMANTIC_SCHEMA extends Attribute { override val name: XmlAttributeName = "semantic" }

  case object SEQUENCE_MAX extends Attribute { override val name: XmlAttributeName = "max" }

  case object SEQUENCE_MIN extends Attribute { override val name: XmlAttributeName = "min" }

  case object SOURCE_ID extends Attribute { override val name: XmlAttributeName = "sid" }

  case object START_SIGN extends Attribute { override val name: XmlAttributeName = "start-sign" }

  case object STOP_SIGN extends Attribute { override val name: XmlAttributeName = "stop-sign" }

  case object STORAGE_PATH extends Attribute { override val name: XmlAttributeName = "storage-path" }

  case object TRIM extends Attribute { override val name: XmlAttributeName = "trim" }

  case object UNIQUE extends Attribute { override val name: XmlAttributeName = "unique" }

  case object XML_ATTRIBUTE_NAME extends Attribute {
    override val name: XmlAttributeName = "xml-attribute-name"
  }

  case object XML_ATTRIBUTE_PARENT extends Attribute {
    override val name: XmlAttributeName = "xml-attribute-parent"
  }

  case object XML_ELEMENT_NAME extends Attribute {
    override val name: XmlAttributeName = "xml-element-name"
  }
}
